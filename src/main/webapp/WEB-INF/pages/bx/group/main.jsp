<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    pageContext.setAttribute("basePath", basePath);
%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<div class="page-header" style="padding:10px 20px;margin:-18px 0px 0px">
</div>
<div class="openAppGrid">
    <div id="spider" class="col-sm-4"></div>
    <div id="bar" class="col-sm-4"></div>
</div>


<script type="text/javascript">
    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    var chart1 = new Highcharts.chart('spider', {
        chart: {
            polar: true,
            type: 'line'
        },
        title: {
            text: '综合评估',
        },
        pane: {
            size: '80%'
        },
        xAxis: {
            categories: ['指挥决策能力', '立体突击能力', '战斗保障能力', '兵力投送能力',
                '信息对抗能力', '火力支援能力'],
            tickmarkPlacement: 'on',
            lineWidth: 0
        },
        yAxis: {
            gridLineInterpolation: 'polygon',
            lineWidth: 0,
            min: 0
        },
        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <br/>'
        },
        credits: {
            enabled: false
        },
        legend: {
            align: 'right',
            verticalAlign: 'top',
            y: 70,
            layout: 'vertical'
        },
        series: [{
            name: '集团1',
            data: [43000, 19000, 60000, 35000, 17000, 10000],
            pointPlacement: 'on'
        }, {
            name: '集团2',
            data: [50000, 39000, 42000, 31000, 26000, 14000],
            pointPlacement: 'on'
        }, {
            name: '集团3',
            data: [10000, 29000, 32000, 21000, 32000, 8000],
            pointPlacement: 'on'
        }]
    });


    var chart2 = Highcharts.chart('bar', {
        chart: {
            type: 'bar'
        },
        title: {
            text: '人员构成'
        },
        xAxis: {
            categories: ['指挥员', '特种兵', '医疗兵', '作战兵', '通信兵', '重火力手'],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '人数',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true,
                    allowOverlap: true // 允许数据标签重叠
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        series: [{
            name: '集团1',
            data: [10, 24, 18, 200, 20, 10]
        }, {
            name: '集团2',
            data: [12, 25, 20, 230, 23, 12]
        }, {
            name: '集团3',
            data: [8, 12, 10, 100, 15, 8]
        }]
    });
</script>