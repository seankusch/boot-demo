<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    pageContext.setAttribute("basePath", basePath);
%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<div class="page-header" style="padding:10px 20px;margin:-18px 0px 0px">
    <button id="startBtn" class="btn btn-labeled btn-success"
            onclick="startMonitor();">开始监控
    </button>
    <button id="stopBtn" class="btn btn-labeled btn-danger"
            onclick="stopMonitor();">停止监控
    </button>
    <button id="receiveBtn" class="btn btn-labeled btn-danger"
            onclick="startReceive();">开始接受数据
    </button>
<%--    <button id="quertPicsBtn" class="btn btn-labeled btn-info"--%>
<%--            onclick="queryPics();">获取图片--%>
<%--    </button>--%>
<%--    <button id="modifyDeviceBtn" class="btn btn-labeled btn-info"--%>
<%--            onclick='javascript:showModal("更新分类", "bx/data/update");'>修改设备阈值--%>
<%--    </button>--%>
</div>
<div class="openAppGrid">
    <div id="containerSTP" class="col-sm-4"></div>
    <div id="containerPRS" class="col-sm-4"></div>
    <div id="containerHAT" class="col-sm-4"></div>
</div>

<div class="panel col-sm-10">
    <div class="panel-heading">图片处理结果</div>
    <div class="panel-body">
        <ul id="ul-pic-outputs" class="autumn-grids">
            <li class="autumn-grid"></li>
            <li class="autumn-grid"></li>
            <li class="autumn-grid"></li>
        </ul>
    </div>
</div>

<script type="text/javascript">
    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    function activeLastPointToolTip(chart) {
        var points = chart.series[0].points;
        chart.tooltip.refresh(points[points.length - 1]);
    }

    // 全局变量
    var currentTime;
    var samplingData = null;
    // 初始值
    var oriValue = {
        name: '数据',
        data: (function () {
            var data = [],
                time = (new Date()).getTime(),
                i;
            for (i = -19; i <= 0; i += 1) {
                data.push({
                    x: time + i * 1000,
                    y: null
                });
            }
            return data;
        }())
    };

    // 步数
    var chart1 = new Highcharts.chart('containerSTP', {
        chart: {
            type: 'spline',
            marginRight: 10,
            events: {
                load: function () {
                    var series = this.series[0],
                        chart = this;
                    activeLastPointToolTip(chart);
                    setInterval(function () {
                        series.addPoint([currentTime, samplingData === null ? 0 : samplingData.stp], true, true);
                        activeLastPointToolTip(chart);
                    }, 1500);
                }
            }
        },
        credits: {
            enabled: false
        },
        title: {
            text: '步数'
        },
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150
        },
        yAxis: {
            title: {
                text: null
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                    Highcharts.numberFormat(this.y, 2);
            }
        },
        legend: {
            enabled: false
        },
        series: [oriValue]
    });

    // 压力
    var chart2 = new Highcharts.chart('containerPRS', {
        chart: {
            type: 'spline',
            marginRight: 10,
            events: {
                load: function () {
                    var series = this.series[0],
                        chart = this;
                    activeLastPointToolTip(chart);
                    setInterval(function () {
                        series.addPoint([currentTime, samplingData === null ? 0 : samplingData.prs], true, true);
                        activeLastPointToolTip(chart);
                    }, 1500);
                }
            }
        },
        credits: {
            enabled: false
        },
        title: {
            text: '压力'
        },
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150
        },
        yAxis: {
            title: {
                text: null
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                    Highcharts.numberFormat(this.y, 2);
            }
        },
        legend: {
            enabled: false
        },
        series: [oriValue]
    });

    // 心率
    var chart3 = new Highcharts.chart('containerHAT', {
        chart: {
            type: 'spline',
            marginRight: 10,
            events: {
                load: function () {
                    var series = this.series[0],
                        chart = this;
                    activeLastPointToolTip(chart);
                    setInterval(function () {
                        series.addPoint([currentTime, samplingData === null ? 0 : samplingData.hat], true, true);
                        activeLastPointToolTip(chart);
                    }, 1500);
                }
            }
        },
        credits: {
            enabled: false
        },
        title: {
            text: '心率'
        },
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150
        },
        yAxis: {
            title: {
                text: null
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                    Highcharts.numberFormat(this.y, 2);
            }
        },
        legend: {
            enabled: false
        },
        series: [oriValue]
    });

    // 全局定时器
    var timer;

    function startMonitor() {
        // 刷新数据
        timer = setInterval(function () {
            $.ajax({
                url: _urlPath + "bx/data/update/test",
                async: true, //改为同步方式
                type: "get",
                dataType: "json",
                success: function (result) {
                    currentTime = (new Date()).getTime(); // 当前时间
                    samplingData = result.data;
                }
            });
            queryPics();
        }, 1500);
    }

    function stopMonitor() {
        clearInterval(timer);
        samplingData = null;
    }


    /**
     *获取图片文件数组
     */
    function queryPics() {
        $.ajax({
            url: _urlPath + "bx/data/pics/query",
            type: "get",
            dataType: 'json',
            success: function (result) {
                if (!result.data.outputs.length) {
                    // alert("结果路径下没有找到图片");
                    return
                }
                //获取到的图片数组处理逻辑方法
                loadResultPics(result.data.outputs);
            },
            error: function (e) {
                console.log(e);
                console.log("获取文件list数组失败，请检查接口服务");
            }
        });
    }

    /**
     * 加载图片，将图片拼成html代码
     */
    function loadResultPics(data) {
        var ul = document.getElementById("ul-pic-outputs")//定位ul
        var size = ul.childNodes.length;
        for (var idx = 0; idx < size; idx++) {
            ul.removeChild(ul.childNodes[0]);
        }
        for (var i = 0; i < data.length; i++) {
            var src = "${ctx}/light-server/light-server/data/ID_001/DroneOutput/" + data[i];
            var li = document.createElement("li"); //创建li
            li.className = "autumn-grid"
            var img = document.createElement("img");
            img.src = src;
            img.className = "img-responsive";
            li.appendChild(img);//将a添加到li
            ul.appendChild(li); //将li添加到ul
        }
    }

    /**
     * 修改设备阈值
     */
    function modifyDevice(req) {
        $.ajax({
            url: _urlPath + "bx/data/device/modify",
            type: "post",
            data: req,
            dataType: 'json',
            success: function (result) {
                alert(result.data)
            },
            error: function (e) {
                console.log(e);
                console.log("修改设备信息失败");
            }
        });
    }

    /**
     *获取图片文件数组
     */
    function startReceive() {
        $.ajax({
            url: _urlPath + "bx/data/device/receive",
            type: "get",
            dataType: 'json',
            success: function (result) {
            },
            error: function (e) {
                console.log(e);
            }
        });
    }

</script>