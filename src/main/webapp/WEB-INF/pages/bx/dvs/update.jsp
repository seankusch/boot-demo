<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<form id="submitForm" class="form-horizontal">
	<input name="id" value="${device.id}" type="text" hidden="hidden">
	
    <div class="form-group">
        <label class="col-sm-3 control-label" for="model"><font color="red">*</font>设备型号：</label>
        <div class="col-sm-8">
            <input class="form-control" type="text" id="model" name="model" value="${device.model}"/>
            <div id="validation-username" class="validate-error help-block"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="sn"><font color="red">*</font>设备SN：</label>
        <div class="col-sm-8">
            <input class="form-control" type="text" id="sn" name="sn" value="${device.sn}"/>
            <div id="validation-username" class="validate-error help-block"></div>
        </div>
    </div>
</form>
<script type="text/javascript">
	submit = function(){
		frmValidate();
		var data = $("#submitForm").serialize();
		ajaxRequest("bx/device/update", data);
	}
</script>