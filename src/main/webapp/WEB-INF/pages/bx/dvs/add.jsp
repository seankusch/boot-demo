<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<form id="submitForm" class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-3 control-label" for="model"><font color="red">*</font>设备名称：</label>
        <div class="col-sm-8">
            <input class="form-control" type="text" id="model" name="model" placeholder="请填写设备型号"/>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="sn"><font color="red">*</font>设备SN：</label>
        <div class="col-sm-8">
            <input class="form-control" type="text" id="sn" name="sn" placeholder="请填写设备SN"/>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="devicetype"><font color="red">*</font>设备类型：</label>
        <div class="col-sm-8">
            <input class="form-control" type="text" id="devicetype" name="devicetype" placeholder="请填写设备类型"/>
        </div>
    </div>
    
</form>
<script type="text/javascript">
	submit = function(){
		frmValidate();
		
		var test = $.scojs_valid('#submitForm',
				{rules: 
					{
                        model: ['not_empty',{'max_length': 50}],
					    sn: ['not_empty',{'max_length': 20}],
                        devicetype: ['not_empty',{'max_length': 20}],
					},
				 wrapper:".form-group"}
		);
		
		if(!test.validate()){
			return;
		}
		
		var data = $("#submitForm").serialize();
		ajaxRequest("bx/device/add", data);
	}
</script>