import sys
import cv2
import socket
from datetime import datetime
import time
import logging
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
from models.with_mobilenet import PoseEstimationWithMobileNet
from demo_edit import run_demo
import numpy as np
import csv
from numpy import linalg as LA
from scipy.signal import butter, lfilter
from collections import deque
from jieba import lcut
# when score_MEWS is greater than 6, intervention is needed in less than 15min
# when score_MEWS is 4 or 5, intervention is needed in less than 30min
# initialize the time
# initTime = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
# initTime = datetime.utcnow().strftime('%Y-%m-%d %H:%S.%f')[:-3]
initTime = datetime.now()

# 指出误判的关键词
misjudge = ['误判', '判断错误', '判断有误', '错误']
# 自行处理的关键词
selfTreatment = ['自行处理', '自行处置', '自己处置', '自己处理', '自己包扎']
# 自行返回的关键词
goToMedCentre = ['自行返回', '自行撤回', '撤回', '回撤', '去救护所']
# 等待救援的关键词
waitForHelp = ['等待救护队', '等待救援', '请来救援', '请速来', '原地等待', '等待']
# 伤部关键词
head = ['头部', '头', '脑震荡', '前额', '后脑', '耳鸣', '头晕', '头骨', '颅脑', '颅骨', '头颅']
arm = ['上肢', '胳膊', '大臂', '小臂', '手臂', '左臂', '手掌', '左手', '右手', '手肘']
torso = ['胸腔', '胸痛', '胸闷', '腹部', '肝脏', '上腹', '下腹', '肚子', '骨盆']
leg = ['下肢', '腿', '大腿', '小腿', '左腿', '右腿', '脚', '膝盖', '左膝', '右膝', '股骨']
# 伤情与伤因关键词
bleed = ['出血', '大出血', '失血', '流血']
fracture = ['骨折', '骨裂', '折断', '断裂', '错位']
blast = ['冲击', '爆炸', '气浪', '炸弹', '迫击炮', '手榴弹', '地雷', '冲击波']
burn = ['烧伤', '灼烧', '火烧', '着火', '烧毁', '灼热', '点燃', '强酸', '强碱', '化学品', '高温']
firearm = ['火器伤', '子弹', '弹丸', '中弹', '中枪', '枪伤', '弹片', '贯穿', '弹道']
crush = ['挤压', '碾压', '碾轧', '轧', '压埋', '重压', '辗压']

status = {'device':'server', \
          'time':initTime, \
          # 以下标签表示能否在岗
          'tag': 0, # 0 无碍，1 暂缓，2 全失，3 不明
          'scoreMEWS':0, #scoreMEWS: 0 to 10
          'flagSport':False, #是否正在运动
          'flagEnvEvent':False, #是否环境异常
          'countQuestion':0, #0, 1, 2, 3，当前询问内容编号
          'flagComa':False,  #昏迷与否
          'flagTargetFound':False,  #画面中有人与否
          'flagTargetUpright':False, #人物站立与否
          'flagException': False,  #人工介入与否
          # 以下为伤情，伤部与伤因
          'head': False,  #头部受伤与否
          'arm': False,  #上肢受伤与否
          'torso': False,  #躯干受伤与否
          'leg': False,  #腿部受伤与否
          'bleed': False,  #是否出血
          'fracture': False,  #是否骨折
          'blast': False,  #是否炸伤
          'burn': False,  #是否烧伤
          'firearm': False,  #是否火器伤
          'crush': False,  #是否挤压伤
          # 误判次数影响 flagSport 调整 scoreMEWS 中的心率一项时的权重
          'countMisjudgment':0, #误判次数
          'feedBack':0, #医师反馈
          'BPM':75
          }
bpm_max, bpm_min = 100, 50

def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y

def get_mews(bpm, blo, blp):
    alpha = 0
    score = 0
    countSport = 0
    history = []
    bpm_max, bpm_min = 100, 50
    with open(csv_output,'r') as f:
        data = csv.reader(f)
        for line in data:
            history.append(line)
    del history[0]
    for slot in history:
        if slot[4]=='True':
            countSport += 1
    percSport = countSport/len(history)
    sport_offset = 10*percSport
    print('In {:.2%} percent of time, the target is actively in sport '.format(percSport))    
    # 心率分
    if bpm > bpm_max+sport_offset+30:
        score += 3
    elif bpm < bpm_min-10 or bpm > bpm_max+sport_offset+10:
        score += 2
    elif bpm < bpm_min or bpm > bpm_max+sport_offset:
        score += 1
    else:
        score += 0
    score = score - alpha*percSport*(bpm>100)
    # 血氧分
    if blo < 0.9:
        score += 2
    elif blo < 0.95:
        score += 1
    else:
        score += 0
    # 收缩压分项
    if blp <= 70:
        score += 3
    elif blp <= 80 or blp >=200:
        score += 2
    elif blp <= 100:
        score += 1
    else:
        score += 0
    return score

def is_sport(acc):
    # acc 
    # filter out the high frequency noise
    acc_smooth = butter_bandpass_filter(data=acc, lowcut=1, highcut=25, fs=100, order=5)
    sport = 0
    if np.std(acc_smooth) >= 0.1:
        sport = 1
    return sport

def is_sport_simple(acc):
    sport = 0
    if np.std(acc) >= 0.1:
        sport=1
    return sport

def is_harsh(prs, tmp, hmd, lx):
    if max(prs)-min(prs)>5 or max(tmp) >60 or max(hmd) > 100 or max(lx)>5000:
        flag = 1
    else:
        flag = 0
    return flag

class extractor(LoggingEventHandler):
    def on_created(self, event):
        time.sleep(0.01)
        super(LoggingEventHandler, self).on_created(event)
        what = 'directory' if event.is_directory else 'file'
        # logging.info("Created %s: %s", what, event.src_path)
        NameExt = event.src_path.split('/')
        if NameExt[-2] == 'DroneInput':
            logging.info("Drone Captured Image analysis...")
            img = cv2.imread(event.src_path, cv2.IMREAD_COLOR)
            img = np.expand_dims(img, axis=0)
            output_path = path+"/DroneOutput/"+"".join(NameExt[-1])
            net = PoseEstimationWithMobileNet()
            target, upright = run_demo(net, img, output_path)
            # write a new line to the output file
            itis = datetime.now()
            status['device'] = 'Drone'
            status['time'] = itis
            status['flagTargetFound'] = bool(target)
            status['flagTargetUpright']= bool(upright)
            with open(csv_output, 'a') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=status.keys())
                writer.writerow(status)
        elif NameExt[-2] == 'HelmetInput':
            logging.info("Helmet Captured Data reading...")
            acc_l2 =[]
            prs = []
            tmp = []
            hmd = []
            lx = []
            hat = []
            blo = []
            blp = []
            badwords = ['THR']
            goodwords = ['TMP', 'ACC', 'PRS', 'HMD', 'LX', 'HAT', 'BLO', 'BLP']
            with open(event.src_path) as f:
                for line in f:
                    if not any (badword in line for badword in badwords) and \
                    any (goodword in line for goodword in goodwords) and len(line)>1:
                        acc = line.split('|')[2].split(':')[1].split(',')
                        acc = [float(a) for a in acc]
                        acc = LA.norm(acc)
                        acc_l2.append(acc)
                        prs.append(float(line.split('|')[5].split(':')[1]))
                        tmp.append(float(line.split('|')[6].split(':')[1]))
                        hmd.append(float(line.split('|')[7].split(':')[1]))
                        lx.append(float(line.split('|')[8].split(':')[1]))
                        hat.append(int(line.split('|')[9].split(':')[1]))
                        blo.append(float(line.split('|')[11].split(':')[1]))
                        blp.append(float(line.split('|')[12].split(':')[1].split(',')[1]))
            logging.info("Start Calculating mews on "+str(len(acc_l2))+" lines of data")
            mews = get_mews(hat[-1], blp[-1], blo[-1])
            if mews >= 5:
                status['countQuestion'] = 1
                send_data = "record/5/检测到异常生理信号，请求语音确认，属实请报编号，否则请回复误判。/"
                udp_socket.sendto(send_data.encode('utf8'),dest_addr)
            logging.info("Start Calculating sport with filter...")
            sport = is_sport(acc_l2)
            logging.info("Start Calculating sport without filter...")
            sport_simple = is_sport_simple(acc_l2)
            logging.info("Sport with filter is "+str(sport)+" Sport without filter is "+str(sport_simple))
            env = is_harsh(prs, tmp, hmd, lx)
            # write a new line to the output file
            itis = datetime.now()
            status['device'] = 'Helmet'
            status['time'] = itis
            status['flagEnvEvent'] = bool(env)
            status['flagSport'] = bool(sport)
            status['scoreMEWS'] = mews
            status['BPM'] = max(hat[-1], 10)
            with open(csv_output, 'a') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=status.keys())
                writer.writerow(status)
        elif NameExt[-2] == 'MicrophoneInput':
            time.sleep(0.01)
            logging.info("Voice analysis...")
            with open(event.src_path) as f:
                response = f.read()
            if status['countQuestion'] == 1:
                print(response.split('|')[1])
                # if the answer to the 1st question is the target's ID
                if response.split('|')[1] in path:
                    print('ID is correct')
                    status['countQuestion'] += 1
                    send_data = "record/5/请选择应对措施：自行处置伤患处，或自行撤回救护所，或原地等待救护队。/"
                    udp_socket.sendto(send_data.encode('utf8'),dest_addr)
                elif response in misjudge:
                    print('Misjudgment')
                    status['countQuestion'] = 0
                    status['countMisjudgment'] += 1
                    bpm_max = max(bpm_max, status['BPM'])
                    bpm_min = min(bpm_min, status['BPM'])
                    print('Beat Per Minute Threshold Updated')
                else:
                    status['flagComa'] = True
                    status['tag'] = 2
                    status['flagException'] = True
            elif status['countQuestion'] == 2:
                if response in selfTreatment:
                    print('Self Treatment')
                    status['tag'] = 1
                elif response in goToMedCentre:
                    print('Go To MedCentre')
                    status['tag'] = 2
                    status['countQuestion'] += 1
                    send_data = "record/5/请描述伤情、伤部和伤因，以便医护人员准备。/"
                    udp_socket.sendto(send_data.encode('utf8'),dest_addr)
                elif response in waitForHelp:
                    print('Wait For Help')
                    status['tag'] = 2
                    status['countQuestion'] += 1
                else:
                    status['flagComa'] = True
                    status['tag'] = 2
                    status['flagException'] = True
            elif status['countQuestion'] == 3:
                response = lcut(response)
                if set(response) & set(head):
                    status['head'] = True
                if set(response) & set(arm):
                    status['arm'] = True
                if set(response) & set(torso):
                    status['torso'] = True
                if set(response) & set(leg):
                    status['leg'] = True
                if set(response) & set(bleed):
                    status['bleed'] = True
                if set(response) & set(fracture):
                    status['fracture'] = True
                if set(response) & set(blast):
                    status['blast'] = True
                if set(response) & set(burn):
                    status['burn'] = True
                if set(response) & set(firearm):
                    status['firearm'] = True
                if set(response) & set(crush):
                    status['crush'] = True
                if not set(response) & set(head+arm+torso+leg+bleed+fracture+blast+burn+firearm+crush):
                    status['flagComa'] = True
                    status['tag'] = 2
                    status['flagException'] = True
            # write a new line to the output file
            itis = datetime.utcnow().strftime('%Y-%m-%d %H:%S.%f')[:-3]
            # itis = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
            status['time'] = itis
            status['device'] = 'Microphone'
            with open(csv_output, 'a') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=status.keys())
                writer.writerow(status)
        elif NameExt[-2] == 'MultimodalOutput':
            logging.info("Multimodal decision updated...")
        elif NameExt[-2] == 'DroneOutput':
            logging.info("Processed Image updated...")
        else:
            logging.info("Unregistered device...")


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s')
                        #datefmt='%Y-%m-%d %H:%M:%S')
    path = sys.argv[1] if len(sys.argv) > 1 else '.'

    # 生成事件处理器对象
    event_handler = extractor()
    csv_output = path+"/MultimodalOutput/MultimodalOutput.csv"
    vocal = lcut('测试用例音乐')
    # 创建套接字
    udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # 发送数据的地址和端口
    dest_addr = ("127.0.0.1",1001)
    # receive ("127.0.0.1",1000)
    try:
        with open(csv_output, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=status.keys())
            writer.writeheader()
            writer.writerow(status)
    except IOError:
        print("I/O error")
    # 生成监控器对象
    observer = Observer()
    # 注册事件处理器，配置监控目录
    observer.schedule(event_handler, path, recursive=True)
    # 监控器启动——创建线程
    observer.start()

    # 以下代码是为了保持主线程运行
    try:
        while True:
            time.sleep(100)
    except KeyboardInterrupt:
        observer.stop()

    # 主线程任务结束之后，进入阻塞状态，一直等待其他的子线程执行结束之后，主线程再终止
