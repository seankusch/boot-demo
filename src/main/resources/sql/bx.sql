SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auth_operation
-- ----------------------------
DROP TABLE IF EXISTS `auth_operation`;
CREATE TABLE `auth_operation` (
  `opid` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `opcode` varchar(20) NOT NULL COMMENT '权限值',
  `opname` varchar(50) NOT NULL COMMENT '权限名称',
  `ophref` varchar(200) DEFAULT NULL COMMENT '权限操作链接',
  `opseq` int(11) NOT NULL DEFAULT '1' COMMENT '显示顺序',
  PRIMARY KEY (`opid`),
  KEY `op_code_index` (`opcode`) USING BTREE,
  KEY `op_name_index` (`opname`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=381 DEFAULT CHARSET=utf8 COMMENT='权限信息表';

-- ----------------------------
-- Records of auth_operation
-- ----------------------------
INSERT INTO `auth_operation` VALUES ('327', '01', '权限管理', '/admin/auth', '1');
INSERT INTO `auth_operation` VALUES ('328', '02', '数据管理', '/bx/device', '1');
INSERT INTO `auth_operation` VALUES ('329', '03', '日志管理', '/admin/log', '1');
INSERT INTO `auth_operation` VALUES ('330', '0101', '查询用户列表', '/admin/user/queryPage', '1');
INSERT INTO `auth_operation` VALUES ('331', '010102', '更新用户页面', '/admin/user/updatePage/{id}', '1');
INSERT INTO `auth_operation` VALUES ('332', '010206', '角色已绑定权限', '/admin/role/hasOpers/{roleid}', '1');
INSERT INTO `auth_operation` VALUES ('333', '010102', '修改用户', '/admin/user/update', '1');
INSERT INTO `auth_operation` VALUES ('334', '0103', '查询权限列表', '/admin/oper/queryPage', '1');
INSERT INTO `auth_operation` VALUES ('335', '010104', '删除用户', '/admin/user/deleteByID/{id}', '1');
INSERT INTO `auth_operation` VALUES ('336', '0101', '用户管理界面', '/admin/user/mainPage', '1');
INSERT INTO `auth_operation` VALUES ('337', '0201', '查询设备列表', '/bx/device/queryPage', '1');
INSERT INTO `auth_operation` VALUES ('338', '010205', '角色权限管理页面', '/admin/role/bindOperPage/{roleid}', '1');
INSERT INTO `auth_operation` VALUES ('339', '0301', '日志管理界面', '/admin/log/mainPage', '1');
INSERT INTO `auth_operation` VALUES ('340', '010101', '添加用户', '/admin/user/add', '1');
INSERT INTO `auth_operation` VALUES ('341', '010104', '修改个人密码', '/admin/user/updatePasswd', '1');
INSERT INTO `auth_operation` VALUES ('342', '020201', '添加士兵', '/bx/class/addPage', '1');
INSERT INTO `auth_operation` VALUES ('343', '0103', '权限管理界面', '/admin/oper/mainPage', '1');
INSERT INTO `auth_operation` VALUES ('344', '0201', '设备界面', '/bx/device/mainPage', '1');
INSERT INTO `auth_operation` VALUES ('345', '010201', '添加角色页面', '/admin/role/addPage', '1');
INSERT INTO `auth_operation` VALUES ('346', '0102', '角色管理界面', '/admin/role/mainPage', '1');
INSERT INTO `auth_operation` VALUES ('347', '020201', '添加士兵', '/bx/class/add', '1');
INSERT INTO `auth_operation` VALUES ('348', '0301', '查询日志列表', '/admin/log/queryPage', '1');
INSERT INTO `auth_operation` VALUES ('349', '010204', '角色绑定用户', '/admin/role/bindUser', '1');
INSERT INTO `auth_operation` VALUES ('350', '010103', '重置用户密码页面', '/admin/user/updatePwdPage', '1');
INSERT INTO `auth_operation` VALUES ('351', '020102', '修改设备', '/bx/device/update', '1');
INSERT INTO `auth_operation` VALUES ('352', '010203', '删除角色', '/admin/role/deleteByID/{id}', '1');
INSERT INTO `auth_operation` VALUES ('353', '010205', '绑定角色权限', '/admin/role/bindOpers', '1');
INSERT INTO `auth_operation` VALUES ('354', '020101', '添加设备', '/bx/device/addPage', '1');
INSERT INTO `auth_operation` VALUES ('355', '010203', '删除设备', '/bx/device/deleteByID/{id}', '1');
INSERT INTO `auth_operation` VALUES ('356', '010202', '更新角色页面', '/admin/role/updatePage/{id}', '1');
INSERT INTO `auth_operation` VALUES ('357', '010101', '添加用户页面', '/admin/user/addPage', '1');
INSERT INTO `auth_operation` VALUES ('358', '010202', '修改角色', '/admin/role/update', '1');
INSERT INTO `auth_operation` VALUES ('359', '010201', '添加角色', '/admin/role/add', '1');
INSERT INTO `auth_operation` VALUES ('360', '010206', '解除角色权限', '/admin/role/unbindOpers', '1');
INSERT INTO `auth_operation` VALUES ('361', '020102', '修改设备页面', '/bx/device/updatePage/{id}', '1');
INSERT INTO `auth_operation` VALUES ('362', '010206', '角色未绑定权限', '/admin/role/noOpers/{roleid}', '1');
INSERT INTO `auth_operation` VALUES ('363', '0001', '系统主界面', '/admin/main', '1');
INSERT INTO `auth_operation` VALUES ('364', '0202', '士兵界面', '/bx/class/mainPage', '1');
INSERT INTO `auth_operation` VALUES ('365', '010104', '修改个人密码页面', '/admin/user/updatePasswdPage', '1');
INSERT INTO `auth_operation` VALUES ('366', '0102', '查询角色列表', '/admin/role/queryPage', '1');
INSERT INTO `auth_operation` VALUES ('367', '010103', '重置用户密码', '/admin/user/updatePwd', '1');
INSERT INTO `auth_operation` VALUES ('368', '020203', '删除士兵', '/bx/class/deleteByID/{id}', '1');
INSERT INTO `auth_operation` VALUES ('369', '020101', '添加设备', '/bx/device/add', '1');
INSERT INTO `auth_operation` VALUES ('370', '020202', '修改士兵页面', '/bx/class/updatePage/{id}', '1');
INSERT INTO `auth_operation` VALUES ('371', '010204', '角色用户管理页面', '/admin/role/bindUserPage/{roleid}', '1');
INSERT INTO `auth_operation` VALUES ('372', '020202', '修改士兵', '/bx/class/update', '1');
INSERT INTO `auth_operation` VALUES ('373', '0202', '查询士兵列表', '/bx/class/queryPage', '1');
INSERT INTO `auth_operation` VALUES ('374', '0401', '查询信息数据列表', '/bx/pd/queryPage', '1');
INSERT INTO `auth_operation` VALUES ('375', '040102', '修改信息数据', '/bx/pd/update', '1');
INSERT INTO `auth_operation` VALUES ('376', '040101', '修改信息数据页面', '/bx/pd/updatePage/{id}', '1');
INSERT INTO `auth_operation` VALUES ('377', '040102', '添加信息数据', '/bx/pd/add', '1');
INSERT INTO `auth_operation` VALUES ('378', '0401', '信息数据管理界面', '/bx/pd/mainPage', '1');
INSERT INTO `auth_operation` VALUES ('379', '0407', '删除信息数据', '/bx/pd/deleteByID/{id}', '1');
INSERT INTO `auth_operation` VALUES ('380', '040101', '添加信息数据界面', '/bx/pd/addPage', '1');

-- ----------------------------
-- Table structure for auth_role
-- ----------------------------
DROP TABLE IF EXISTS `auth_role`;
CREATE TABLE `auth_role` (
  `roleid` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `rolename` varchar(20) NOT NULL COMMENT '角色名称',
  `cname` varchar(50) NOT NULL COMMENT '中文名',
  `enabled` varchar(2) DEFAULT NULL COMMENT '是否有效：0无效；1有效',
  PRIMARY KEY (`roleid`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='角色信息表';

-- ----------------------------
-- Records of auth_role
-- ----------------------------
INSERT INTO `auth_role` VALUES ('23', 'admin', '超级管理员', '1');
INSERT INTO `auth_role` VALUES ('25', 'anonymous', '匿名用户组', '1');

-- ----------------------------
-- Table structure for auth_role_operation
-- ----------------------------
DROP TABLE IF EXISTS `auth_role_operation`;
CREATE TABLE `auth_role_operation` (
  `roleid` int(11) NOT NULL COMMENT '角色ID',
  `opid` int(11) NOT NULL COMMENT '权限ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色权限关系表';

-- ----------------------------
-- Records of auth_role_operation
-- ----------------------------
INSERT INTO `auth_role_operation` VALUES ('23', '100');
INSERT INTO `auth_role_operation` VALUES ('23', '101');
INSERT INTO `auth_role_operation` VALUES ('23', '102');
INSERT INTO `auth_role_operation` VALUES ('23', '103');
INSERT INTO `auth_role_operation` VALUES ('23', '104');
INSERT INTO `auth_role_operation` VALUES ('23', '105');
INSERT INTO `auth_role_operation` VALUES ('23', '106');
INSERT INTO `auth_role_operation` VALUES ('23', '107');
INSERT INTO `auth_role_operation` VALUES ('23', '108');
INSERT INTO `auth_role_operation` VALUES ('23', '109');
INSERT INTO `auth_role_operation` VALUES ('23', '110');
INSERT INTO `auth_role_operation` VALUES ('23', '111');
INSERT INTO `auth_role_operation` VALUES ('23', '112');
INSERT INTO `auth_role_operation` VALUES ('23', '113');
INSERT INTO `auth_role_operation` VALUES ('23', '114');
INSERT INTO `auth_role_operation` VALUES ('23', '115');
INSERT INTO `auth_role_operation` VALUES ('23', '116');
INSERT INTO `auth_role_operation` VALUES ('23', '117');
INSERT INTO `auth_role_operation` VALUES ('23', '118');
INSERT INTO `auth_role_operation` VALUES ('23', '119');
INSERT INTO `auth_role_operation` VALUES ('23', '120');
INSERT INTO `auth_role_operation` VALUES ('23', '121');
INSERT INTO `auth_role_operation` VALUES ('23', '122');
INSERT INTO `auth_role_operation` VALUES ('23', '123');
INSERT INTO `auth_role_operation` VALUES ('23', '124');
INSERT INTO `auth_role_operation` VALUES ('23', '125');
INSERT INTO `auth_role_operation` VALUES ('23', '126');
INSERT INTO `auth_role_operation` VALUES ('23', '127');
INSERT INTO `auth_role_operation` VALUES ('23', '128');
INSERT INTO `auth_role_operation` VALUES ('23', '200');
INSERT INTO `auth_role_operation` VALUES ('23', '201');
INSERT INTO `auth_role_operation` VALUES ('23', '202');
INSERT INTO `auth_role_operation` VALUES ('23', '203');
INSERT INTO `auth_role_operation` VALUES ('23', '204');
INSERT INTO `auth_role_operation` VALUES ('23', '205');
INSERT INTO `auth_role_operation` VALUES ('23', '206');
INSERT INTO `auth_role_operation` VALUES ('23', '300');
INSERT INTO `auth_role_operation` VALUES ('23', '301');
INSERT INTO `auth_role_operation` VALUES ('23', '302');
INSERT INTO `auth_role_operation` VALUES ('23', '319');
INSERT INTO `auth_role_operation` VALUES ('23', '320');
INSERT INTO `auth_role_operation` VALUES ('23', '321');
INSERT INTO `auth_role_operation` VALUES ('23', '322');
INSERT INTO `auth_role_operation` VALUES ('23', '323');
INSERT INTO `auth_role_operation` VALUES ('23', '324');
INSERT INTO `auth_role_operation` VALUES ('23', '325');
INSERT INTO `auth_role_operation` VALUES ('23', '326');
INSERT INTO `auth_role_operation` VALUES ('23', '327');
INSERT INTO `auth_role_operation` VALUES ('23', '328');
INSERT INTO `auth_role_operation` VALUES ('23', '329');
INSERT INTO `auth_role_operation` VALUES ('23', '363');
INSERT INTO `auth_role_operation` VALUES ('23', '330');
INSERT INTO `auth_role_operation` VALUES ('23', '336');
INSERT INTO `auth_role_operation` VALUES ('23', '340');
INSERT INTO `auth_role_operation` VALUES ('23', '357');
INSERT INTO `auth_role_operation` VALUES ('23', '331');
INSERT INTO `auth_role_operation` VALUES ('23', '333');
INSERT INTO `auth_role_operation` VALUES ('23', '350');
INSERT INTO `auth_role_operation` VALUES ('23', '367');
INSERT INTO `auth_role_operation` VALUES ('23', '335');
INSERT INTO `auth_role_operation` VALUES ('23', '341');
INSERT INTO `auth_role_operation` VALUES ('23', '365');
INSERT INTO `auth_role_operation` VALUES ('23', '346');
INSERT INTO `auth_role_operation` VALUES ('23', '366');
INSERT INTO `auth_role_operation` VALUES ('23', '345');
INSERT INTO `auth_role_operation` VALUES ('23', '359');
INSERT INTO `auth_role_operation` VALUES ('23', '356');
INSERT INTO `auth_role_operation` VALUES ('23', '358');
INSERT INTO `auth_role_operation` VALUES ('23', '352');
INSERT INTO `auth_role_operation` VALUES ('23', '355');
INSERT INTO `auth_role_operation` VALUES ('23', '349');
INSERT INTO `auth_role_operation` VALUES ('23', '371');
INSERT INTO `auth_role_operation` VALUES ('23', '338');
INSERT INTO `auth_role_operation` VALUES ('23', '353');
INSERT INTO `auth_role_operation` VALUES ('23', '332');
INSERT INTO `auth_role_operation` VALUES ('23', '360');
INSERT INTO `auth_role_operation` VALUES ('23', '362');
INSERT INTO `auth_role_operation` VALUES ('23', '334');
INSERT INTO `auth_role_operation` VALUES ('23', '343');
INSERT INTO `auth_role_operation` VALUES ('23', '337');
INSERT INTO `auth_role_operation` VALUES ('23', '344');
INSERT INTO `auth_role_operation` VALUES ('23', '354');
INSERT INTO `auth_role_operation` VALUES ('23', '369');
INSERT INTO `auth_role_operation` VALUES ('23', '351');
INSERT INTO `auth_role_operation` VALUES ('23', '361');
INSERT INTO `auth_role_operation` VALUES ('23', '364');
INSERT INTO `auth_role_operation` VALUES ('23', '373');
INSERT INTO `auth_role_operation` VALUES ('23', '342');
INSERT INTO `auth_role_operation` VALUES ('23', '347');
INSERT INTO `auth_role_operation` VALUES ('23', '370');
INSERT INTO `auth_role_operation` VALUES ('23', '372');
INSERT INTO `auth_role_operation` VALUES ('23', '368');
INSERT INTO `auth_role_operation` VALUES ('23', '339');
INSERT INTO `auth_role_operation` VALUES ('23', '348');
INSERT INTO `auth_role_operation` VALUES ('23', '374');
INSERT INTO `auth_role_operation` VALUES ('23', '378');
INSERT INTO `auth_role_operation` VALUES ('23', '376');
INSERT INTO `auth_role_operation` VALUES ('23', '380');
INSERT INTO `auth_role_operation` VALUES ('23', '375');
INSERT INTO `auth_role_operation` VALUES ('23', '377');
INSERT INTO `auth_role_operation` VALUES ('23', '379');

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `username` char(20) NOT NULL,
  `password` char(32) NOT NULL,
  `mobilephone` varchar(20) DEFAULT NULL COMMENT '手机号码',
  `idcard` varchar(18) DEFAULT NULL COMMENT '身份证号码',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  `email` varchar(100) DEFAULT NULL,
  `useable` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否可用(0禁用,1可用)',
  `roleid` int(11) NOT NULL COMMENT '所属角色',
  `addtime` datetime NOT NULL COMMENT '创建时间',
  `logintime` datetime DEFAULT NULL COMMENT '登陆时间',
  `loginip` varchar(15) DEFAULT NULL COMMENT '登陆IP',
  `enabled` varchar(2) DEFAULT '1' COMMENT '是否有效：0无效；1有效',
  PRIMARY KEY (`id`),
  KEY `username` (`username`) USING BTREE,
  KEY `fk_user_role` (`roleid`),
  CONSTRAINT `fk_user_role` FOREIGN KEY (`roleid`) REFERENCES `auth_role` (`roleid`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user
-- ----------------------------
INSERT INTO `auth_user` VALUES ('18', 'admin', 'e10adc3949ba59abbe56e057f20f883e', null, null, null, null, '1', '23', '2017-04-21 14:45:05', '2018-04-12 18:53:37', '36.57.33.250', '1');

-- ----------------------------
-- Table structure for tbl_soldier
-- ----------------------------
DROP TABLE IF EXISTS `tbl_soldier`;
CREATE TABLE `tbl_soldier` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(200) DEFAULT NULL COMMENT '分类名称',
  `orderIndex` int(20) DEFAULT NULL COMMENT '显示顺序',
  `enabled` varchar(2) DEFAULT NULL COMMENT '是否有效：0无效；1有效',
  `createtime` datetime DEFAULT NULL COMMENT '添加时间',
  `createuser` varchar(50) DEFAULT NULL COMMENT '添加人',
  `updatetime` datetime DEFAULT NULL COMMENT '修改时间',
  `updateuser` varchar(50) DEFAULT NULL COMMENT '修改人',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='士兵表';

-- ----------------------------
-- Records of tbl_soldier
-- ----------------------------
INSERT INTO `tbl_soldier` VALUES ('1', '李龙', '1', '1', '2018-04-06 21:06:35', '1', '2018-04-10 01:53:14', '18', null);
INSERT INTO `tbl_soldier` VALUES ('2', '肖垚', '2', '1', '2018-04-06 21:28:42', '18', null, null, null);
INSERT INTO `tbl_soldier` VALUES ('3', '张晖', '3', '1', '2018-04-06 21:28:51', '18', null, null, null);

-- ----------------------------
-- Table structure for tbl_device
-- ----------------------------
DROP TABLE IF EXISTS `tbl_device`;
CREATE TABLE `tbl_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `model` varchar(200) DEFAULT NULL COMMENT '设备型号',
  `sn` varchar(20) DEFAULT NULL COMMENT '设备SN',
  `devicetype` varchar(20) DEFAULT NULL COMMENT '类型',
  `enabled` varchar(2) DEFAULT NULL COMMENT '是否有效：0无效；1有效',
  `createtime` datetime DEFAULT NULL COMMENT '添加时间',
  `createuser` varchar(50) DEFAULT NULL COMMENT '添加人',
  `updatetime` datetime DEFAULT NULL COMMENT '修改时间',
  `updateuser` varchar(50) DEFAULT NULL COMMENT '修改人',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='设备表';

-- ----------------------------
-- Records of tbl_device
-- ----------------------------
INSERT INTO `tbl_device` VALUES ('1', '小米', '1111111', '手环', '1', '2017-05-04 15:22:46', null, '2017-05-04 15:22:46', null, null);
INSERT INTO `tbl_device` VALUES ('2', '华为', '2222222', '手环', '1','2018-04-06 00:00:34', null, null, null, null);
INSERT INTO `tbl_device` VALUES ('3', '华为', '3333333', '手环', '1','2018-04-06 00:07:12', null, null, null, null);
INSERT INTO `tbl_device` VALUES ('4', '大疆', '4444444', '头盔', '1','2018-04-06 00:16:46', '18', null, null, null);
INSERT INTO `tbl_device` VALUES ('5', '大疆', '5555555', '头盔', '1','2018-04-06 00:19:59', '18', '2018-04-10 01:53:04', '18', null);
INSERT INTO `tbl_device` VALUES ('6', '大疆', '6666666', '头盔', '1','2018-04-06 00:23:43', '18', '2018-04-06 00:38:24', '18', null);


-- ----------------------------
-- Table structure for tbl_data
-- ----------------------------
DROP TABLE IF EXISTS `tbl_data`;
CREATE TABLE `tbl_data` (
  `DATA_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键DATA_ID',
  `ID` varchar(50) DEFAULT NULL COMMENT 'ID',
  `SEQ` BIGINT DEFAULT NULL COMMENT '采样序号',
  `UUID` BIGINT DEFAULT NULL COMMENT '时间戳',
  `ACC_X` FLOAT DEFAULT NULL COMMENT '加速度X',
  `ACC_Y` FLOAT DEFAULT NULL COMMENT '加速度Y',
  `ACC_Z` FLOAT DEFAULT NULL COMMENT '加速度Z',
  `GRY_X` FLOAT DEFAULT NULL COMMENT '角速度X',
  `GRY_Y` FLOAT DEFAULT NULL COMMENT '角速度Y',
  `GRY_Z` FLOAT DEFAULT NULL COMMENT '角速度Z',
  `MAG_X` FLOAT DEFAULT NULL COMMENT '磁力计X',
  `MAG_Y` FLOAT DEFAULT NULL COMMENT '磁力计Y',
  `MAG_Z` FLOAT DEFAULT NULL COMMENT '磁力计Z',
  `PRS` FLOAT DEFAULT NULL COMMENT '压力',
  `TMP` FLOAT DEFAULT NULL COMMENT '温度',
  `HMD` FLOAT DEFAULT NULL COMMENT '湿度',
  `LX` FLOAT DEFAULT NULL COMMENT '光照',
  `HAT` TINYINT DEFAULT NULL COMMENT '心率',
  `BLS` FLOAT DEFAULT NULL COMMENT '血糖',
  `BLO` FLOAT DEFAULT NULL COMMENT '血氧',
  `BLP_L` FLOAT DEFAULT NULL COMMENT '血压(低)',
  `BLP_H` FLOAT DEFAULT NULL COMMENT '血压(高)',
  `STP` BIGINT DEFAULT NULL COMMENT '步数',
  `CAL` FLOAT DEFAULT NULL COMMENT '卡路里',
  `GPS_1` varchar(50) DEFAULT NULL COMMENT 'GPS1',
  `GPS_2` varchar(50) DEFAULT NULL COMMENT 'GPS2',
  PRIMARY KEY (`DATA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='数据表';

-- ----------------------------
-- Table structure for bxproducts
-- ----------------------------
DROP TABLE IF EXISTS `bxproducts`;
CREATE TABLE `bxproducts` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(200) DEFAULT NULL COMMENT '产品名称',
  `soldierId` int(11) DEFAULT NULL COMMENT '所属士兵',
  `deviceId` int(11) DEFAULT NULL COMMENT '所属设备',
  `productCode` varchar(20) DEFAULT NULL COMMENT '险种代码',
  `pic_1` varchar(200) DEFAULT NULL COMMENT '产品图片1',
  `pic_2` varchar(200) DEFAULT NULL COMMENT '产品图片2',
  `brief` varchar(1024) DEFAULT NULL COMMENT '产品简介',
--   `isHot` bit(1) DEFAULT NULL COMMENT '是否热门产品:0否；1是',
  `productTag` varchar(50) DEFAULT NULL COMMENT '产品标签',
--   `price` decimal(18,2) DEFAULT NULL COMMENT '产品价格',
  `orderIndex` int(11) DEFAULT NULL COMMENT '显示顺序',
  `createDateTime` datetime DEFAULT NULL COMMENT '添加时间',
  `publishDateTime` datetime DEFAULT NULL COMMENT '发布时间',
  `status` int(11) DEFAULT NULL COMMENT '产品状态：0：下线1：上线',
--   `saleTotal` int(11) DEFAULT NULL COMMENT '销量',
  `url_1` varchar(255) DEFAULT NULL COMMENT '地址1',
  `url_2` varchar(255) DEFAULT NULL COMMENT '地址2',
  `url_3` varchar(255) DEFAULT NULL COMMENT '地址3',
  `url_4` varchar(255) DEFAULT NULL COMMENT '地址4',
  `enabled` varchar(2) DEFAULT NULL COMMENT '是否有效：0无效；1有效',
  `createuser` varchar(50) DEFAULT NULL COMMENT '添加人',
  `updatetime` datetime DEFAULT NULL COMMENT '修改时间',
  `updateuser` varchar(50) DEFAULT NULL COMMENT '修改人',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='信息数据表';

-- ----------------------------
-- Records of bxproducts
-- ----------------------------
-- INSERT INTO `bxproducts` VALUES ('2', '境外旅游险', '2', '1', '0649', '/upload/4e6baef2-cc67-4a56-89c9-37b9df9130d5.jpg', '/upload/633f843e-10c6-425e-9b26-6c72513a7ee0.jpg', '亚洲旅行保障，境外旅行首选', '', '热门', '43.80', '1', '2018-04-10 17:29:31', '2018-04-12 06:10:53', '1', '12', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0649_jwly&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0649_jwly&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0649_jwly&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0649_jwly&isMulti=1&extenterpCode=0105100748&issueChannel=4', '1', '18', '2018-04-12 06:10:53', '18', null);
-- INSERT INTO `bxproducts` VALUES ('3', '境内旅游意外险', '2', '1', '0674', '/upload/8089228a-662f-4f7b-8e52-a70e53854692.jpg', '/upload/f3381aed-ac14-410f-886d-e98a39908b56.jpg', '带上她，来一场说走就走的旅行', '', '精选', '4.80', '2', '2018-04-10 17:47:46', '2018-04-12 06:10:20', '1', '2', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0674_micro&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0674_micro&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0674_micro&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0674_micro&isMulti=1&extenterpCode=0105100748&issueChannel=4', '1', '18', '2018-04-12 06:10:20', '18', null);
-- INSERT INTO `bxproducts` VALUES ('4', '一路平安航空意外险', '2', '1', '060a', '/upload/c6a74685-2e86-4476-990b-c7a04b2d21b8.jpg', '/upload/883c47f7-736b-4595-a5ad-cfc92338ca3e.jpg', '给家人一份安心和承诺，覆盖全球所有航班', '', '', '4.97', '3', '2018-04-11 02:31:00', '2018-04-11 02:34:09', '1', '1', 'https://mtest.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=060a_hkyw&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://mtest.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=060a_hkyw&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://mtest.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=060a_hkyw&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://mtest.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=060a_hkyw&isMulti=1&extenterpCode=0105100748&issueChannel=4', '1', '18', '2018-04-11 02:34:09', '18', null);
-- INSERT INTO `bxproducts` VALUES ('5', '“君安保”个人意外伤害保险', '2', '1', '0615', '/upload/fc846588-60de-45aa-95f6-a738c5ffde96.jpg', '/upload/817df2dc-3f07-4728-a412-d76da1e5edd5.jpg', '给自己或家人一份全方位的综合意外保障', '\0', '', '100.00', '5', '2018-04-11 02:45:07', '2018-04-11 02:45:07', '1', '0', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0615_jab&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0615_jab&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0615_jab&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0615_jab&isMulti=1&extenterpCode=0105100748&issueChannel=4', '1', '18', '2018-04-11 02:45:07', null, null);
-- INSERT INTO `bxproducts` VALUES ('6', '“尊享人生”人身意外伤害综合保险', '2', '1', '06A9', '/upload/a6c70fa2-5336-482c-bbfa-d84bd0b8ee9e.jpg', '/upload/5f62537c-02ef-48ba-957e-319474151d9e.jpg', '百万意外保障，不限社保，全面分担医疗费压力', '\0', '', '89.00', '10', '2018-04-11 02:47:46', '2018-04-11 02:47:46', '1', '0', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=06A9&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=06A9&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=06A9&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=06A9&isMulti=1&extenterpCode=0105100748&issueChannel=4', '1', '18', '2018-04-11 02:47:46', null, null);
-- INSERT INTO `bxproducts` VALUES ('7', '预防接种意外伤害险', '2', '1', '0615', '/upload/d63c3c85-e682-4174-b18c-b54addd51fd9.jpg', '/upload/8b80bc1a-e1b0-4251-93a8-7c4bee97c312.jpg', '全面保障宝宝接种疫苗的风险', '\0', '', '25.00', '20', '2018-04-11 02:49:26', '2018-04-11 06:51:17', '1', '0', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0615_ym&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0615_ym&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0615_ym&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0615_ym&isMulti=1&extenterpCode=0105100748&issueChannel=4', '1', '18', '2018-04-11 03:12:11', '18', null);
-- INSERT INTO `bxproducts` VALUES ('8', '学生险', '2', '1', '0606', '/upload/7b466639-ecb1-4ecd-9639-bda65e7b7bf1.jpg', '/upload/d665686c-46b0-42b8-8ecb-5c39f4d61c63.jpg', '学生人身意外伤害保险', '', '', '100.00', '20', '2018-04-11 02:51:21', '2018-04-11 02:51:32', '1', '0', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0606_nngd&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0606_nngd&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0606_nngd&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0606_nngd&isMulti=1&extenterpCode=0105100748&issueChannel=4', '1', '18', '2018-04-11 02:51:32', '18', null);
-- INSERT INTO `bxproducts` VALUES ('9', '一路平安交通意外伤害保险', '2', '1', '060A', '/upload/087d08f4-852c-4931-b2da-7aa8ba6eebf3.jpg', '/upload/1c4b17ad-9268-419a-aa52-43f6cc22c2f1.jpg', '100万高额保障，超10种交通工具', '\0', '', '11.00', '40', '2018-04-11 02:53:54', '2018-04-11 02:53:54', '1', '0', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=060A_0804&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=060A_0804&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=060A_0804&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=060A_0804&isMulti=1&extenterpCode=0105100748&issueChannel=4', '1', '18', '2018-04-11 02:53:54', null, null);
-- INSERT INTO `bxproducts` VALUES ('10', '广西家车宝驾乘人员意外险', '2', '1', '060I', '/upload/09f1e928-3a8b-49da-9331-213311a9bc9a.jpg', '/upload/c3e079b4-5007-477a-aeba-415f41483aa2.jpg', '一单保全家', '\0', '', '2000.00', '50', '2018-04-11 02:55:11', '2018-04-12 06:12:32', '1', '1', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=060I&productPlanId=4271e6a5-7f0b-458d-9cec-c302d8d52a35&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=060I&productPlanId=4271e6a5-7f0b-458d-9cec-c302d8d52a35&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=060I&productPlanId=4271e6a5-7f0b-458d-9cec-c302d8d52a35&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=060I&productPlanId=4271e6a5-7f0b-458d-9cec-c302d8d52a35&extenterpCode=0105100748&issueChannel=4', '1', '18', '2018-04-12 06:12:32', '18', null);
-- INSERT INTO `bxproducts` VALUES ('11', '貌美如花综合意外险', '2', '1', '060J', '/upload/8f20a9f6-1554-440a-b49e-0d6e6fddc06d.jpg', '/upload/bab92012-6b4d-4804-9909-9400b7f48b44.jpg', '这看脸的世界，就该貌美如花', '\0', '', '97.00', '60', '2018-04-11 02:56:07', '2018-04-12 06:00:47', '1', '0', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=060J_mmrh&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=060J_mmrh&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=060J_mmrh&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=060J_mmrh&isMulti=1&extenterpCode=0105100748&issueChannel=4', '1', '18', '2018-04-12 06:00:47', '18', null);
-- INSERT INTO `bxproducts` VALUES ('12', '吃货无忧保障计划', '3', '1', '1227', '/upload/043bf414-0160-4558-a341-40d5094bb944.jpg', '/upload/dc3cc72a-f9a2-495d-aa4f-d9801bac3133.jpg', '吃货肠胃保障，全年最低只需15元', '\0', '', '15.00', '70', '2018-04-11 02:58:40', '2018-04-11 02:58:40', '1', '0', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=1227_chwy&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=1227_chwy&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=1227_chwy&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=1227_chwy&isMulti=1&extenterpCode=0105100748&issueChannel=4', '1', '18', '2018-04-11 02:58:40', null, null);
-- INSERT INTO `bxproducts` VALUES ('13', '驾校学员意外险', '2', '1', '0695', '/upload/54b857ac-a13c-43a5-b309-dd6254001527.jpg', '/upload/232afd4a-d2ca-4620-80e4-8228e61f88d2.jpg', '学车难免有磕碰，35元尽享安心保障', '\0', '', '35.00', '90', '2018-04-11 03:00:22', '2018-04-11 09:22:24', '1', '0', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0695_micro&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0695_micro&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0695_micro&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0695_micro&isMulti=1&extenterpCode=0105100748&issueChannel=4', '1', '18', '2018-04-11 06:53:01', '18', null);
-- INSERT INTO `bxproducts` VALUES ('14', '食客安心餐饮业综合保险', '3', '1', '1006', '/upload/5432c1f0-0c69-466b-b13c-5ee704936326.jpg', '/upload/e748034c-01e1-44d0-8317-4df6a5cd764c.jpg', '餐饮经营者的救兵，老板事业的护身符', '\0', '', '0.00', '100', '2018-04-11 03:02:01', '2018-04-12 06:11:04', '1', '0', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=1006_micro&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=1006_micro&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=1006_micro&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=1006_micro&isMulti=1&extenterpCode=0105100748&issueChannel=4', '1', '18', '2018-04-12 06:11:04', '18', null);
-- INSERT INTO `bxproducts` VALUES ('15', '监护人责任险', '2', '1', '0448', '/upload/2a639ceb-c9f6-416a-bfbc-b9179517c798.jpg', '/upload/fda57f90-53d1-43c7-b1ba-2c50bf815a3e.jpg', '拥有熊孩子调皮捣蛋险，从此不再担心熊孩子闯祸', '\0', '', '48.00', '120', '2018-04-11 03:03:42', '2018-04-11 09:29:45', '1', '0', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0448_jhr&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0448_jhr&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0448_jhr&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0448_jhr&isMulti=1&extenterpCode=0105100748&issueChannel=4', '1', '18', '2018-04-11 09:29:45', '18', null);
-- INSERT INTO `bxproducts` VALUES ('16', '扶老人险', '2', '1', '0458', '/upload/b4131055-ba89-4301-80ce-ca10346f0867.jpg', '/upload/2d10efed-c4d1-4335-9468-724f725d4bea.jpg', '保障您的正义行为', '\0', '', '3.00', '130', '2018-04-11 03:04:52', '2018-04-11 03:04:52', '1', '0', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0458_flr&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0458_flr&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0458_flr&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0458_flr&isMulti=1&extenterpCode=0105100748&issueChannel=4', '1', '18', '2018-04-11 03:04:52', null, null);
-- INSERT INTO `bxproducts` VALUES ('17', '个人账户资金损失险', '3', '1', '0840', '/upload/723f7e64-5304-4df5-b7b3-53cfdf770f4f.jpg', '/upload/a0fa7e40-6941-4928-ab8e-f50e8dc5a0bd.jpg', '黑手无处不在，13元尽享“资金”保险箱', '\0', '', '13.00', '150', '2018-04-11 03:06:14', '2018-04-12 06:11:10', '1', '0', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0840_micro&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0840_micro&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0840_micro&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0840_micro&isMulti=1&extenterpCode=0105100748&issueChannel=4', '1', '18', '2018-04-12 06:11:10', '18', null);
-- INSERT INTO `bxproducts` VALUES ('18', '电动车第三者责任险', '2', '1', '0439', '/upload/dcee3c4d-a4cf-4c45-935a-4b4c3b66c2a0.jpg', '/upload/d2a9be50-70ae-492d-bd20-f7ed93817c26.jpg', '碰瓷、被碰不用怕，过失碰撞有保障', '\0', '', '30.00', '160', '2018-04-11 03:07:33', '2018-04-11 03:07:33', '1', '0', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0439&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0439&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0439&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0439&isMulti=1&extenterpCode=0105100748&issueChannel=4', '1', '18', '2018-04-11 03:07:33', null, null);
-- INSERT INTO `bxproducts` VALUES ('19', '广西满堂福家庭财产保险', '1', '1', '0833', '/upload/919f1712-29ea-4ed1-aef7-b16c351a24b8.jpg', '/upload/6127403b-3775-4fa6-b42c-119751de89e0.jpg', '小小的投入，拖起一个安全的家', '\0', '', '227.50', '180', '2018-04-11 03:08:29', '2018-04-12 00:21:30', '1', '0', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0833_micro&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0833_micro&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0833_micro&isMulti=1&extenterpCode=0105100748&issueChannel=4', 'https://m.sinosafe.com.cn/micro-plat/app/policy-common-comp-page.html?productId=0833_micro&isMulti=1&extenterpCode=0105100748&issueChannel=4', '1', '18', '2018-04-12 00:21:30', '18', null);

-- ----------------------------
-- Table structure for i_log
-- ----------------------------
DROP TABLE IF EXISTS `i_log`;
CREATE TABLE `i_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL COMMENT '操作用户',
  `type` tinyint(4) NOT NULL COMMENT '操作类型(0操作日志;1异常日志)',
  `url` varchar(255) DEFAULT NULL COMMENT '请求地址',
  `method` varchar(255) NOT NULL COMMENT '执行方法',
  `params` text COMMENT '请求参数',
  `requestip` varchar(255) NOT NULL COMMENT '请求IP',
  `description` varchar(255) DEFAULT NULL COMMENT '操作描述',
  `detail` text COMMENT '异常详情',
  `oper_date` datetime NOT NULL COMMENT '操作日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3277 DEFAULT CHARSET=utf8 COMMENT='系统日志表';

-- ----------------------------
-- Procedure structure for addOrUpdateOper
-- ----------------------------
DROP PROCEDURE IF EXISTS `addOrUpdateOper`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `addOrUpdateOper`(IN `opcode` varchar(20),IN `ophref` varchar(50),IN `opname` varchar(50),IN `opseq` int)
BEGIN
	declare opid int default 0;
	select op.opid intoopid from auth_operation AS op where op.opcode = opcode and op.ophref = ophref;
	if opid > 0 then
		update auth_operation set opname = opname, opseq = opseq 
		where opcode = opcode and ophref = ophref;
		delete from auth_operation where opid = opid;
	else
		insert into auth_operation(opcode, opname, ophref, opseq) 
		values(opcode, opname, ophref, opseq);
	end if;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for bind_opers
-- ----------------------------
DROP PROCEDURE IF EXISTS `bind_opers`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bind_opers`(IN `roleid` integer)
BEGIN
	/*用于判断是否结束循环*/
	declare done int default 0;
	declare opid int;
	declare count int default 0;
	/*定义游标*/
	declare c_opid cursor for select op.opid from auth_operation op;
	/*定义 设置循环结束标识done值怎么改变 的逻辑*/
	declare continue handler for not FOUND set done = 1;
	/*打开游标*/
	open c_opid;
		repeat
			fetch c_opid into opid;
			if not done then 
				select count(op.opid) into count from auth_role_operation op where op.roleid = roleid and op.opid = opid;

				if count <= 0 then
					insert into auth_role_operation(roleid, opid) values(roleid, opid);
				end if;
			end if;
		until done end repeat;
	close c_opid;  /*关闭游标*/
END
;;
DELIMITER ;
