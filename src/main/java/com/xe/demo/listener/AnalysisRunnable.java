package com.xe.demo.listener;

import com.xe.demo.common.utils.RunShellUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AnalysisRunnable implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(AnalysisRunnable.class);

    @Override
    public void run() {
        logger.error("analysis starting....");
        RunShellUtil util = new RunShellUtil();
        util.exec("python /usr/local/apache-tomcat-8.5.58/webapps/demo/light-server/light-server/server_LoggingEventHandler.py /usr/local/apache-tomcat-8.5.58/webapps/demo/light-server/light-server/data/ID_001/");
        logger.error("analysis started");
    }
}
