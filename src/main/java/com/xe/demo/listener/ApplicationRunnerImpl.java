package com.xe.demo.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class ApplicationRunnerImpl implements ApplicationRunner {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationRunnerImpl.class);

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        logger.error("Starting............................");
        AnalysisRunnable analysisRunnable = new AnalysisRunnable();
        Thread thread = new Thread(analysisRunnable);
        thread.setName("light-server");
        thread.start();
        logger.error("Started !!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    }
}