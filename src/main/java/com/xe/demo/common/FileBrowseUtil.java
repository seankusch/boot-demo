package com.xe.demo.common;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

/**
 * 文件预览辅助类
 */
public class FileBrowseUtil {
    /**
     * 通过递归得到某一路径下所有的目录及其文件
     *
     * @param filePath 文件路径
     * @return
     */
    public static ArrayList<String> getFiles(String filePath) {
        ArrayList<String> fileList = new ArrayList<>();
        File root = new File(filePath);
        File[] files = root.listFiles();
        Arrays.sort(files, new Comparator<File>() {
            public int compare(File f1, File f2) {
                long diff = f1.lastModified() - f2.lastModified();
                if (diff > 0)
                    return 1;
                else if (diff == 0)
                    return 0;
                else
                    return -1;//如果 if 中修改为 返回-1 同时此处修改为返回 1  排序就会是递减
            }
        });
        for (File file : files) {
            String picPathStr = file.getName();
            fileList.add(picPathStr);
        }

        return fileList;
    }
}
