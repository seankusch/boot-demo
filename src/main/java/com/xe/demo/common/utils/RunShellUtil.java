package com.xe.demo.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class RunShellUtil {
    private static final Logger logger = LoggerFactory.getLogger(RunShellUtil.class);

     class StreamGobbler extends Thread {
        private InputStream is;
        private String type; //输出流的类型ERROR或OUTPUT
        private List<String> result = new ArrayList<>();

        StreamGobbler(InputStream is, String type) {
            this.is = is;
            this.type = type;
        }

        public void run() {
            try {
                InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
                BufferedReader br = new BufferedReader(isr);
                String line = null;
                while ((line = br.readLine()) != null) {
                    result.add(new String(line.getBytes("UTF-8")));
                    logger.error(type + ">" + line);
                }
            } catch (IOException ioe) {
                logger.error(type + ">" + ioe);
                ioe.printStackTrace();
            }
        }

        public List<String> getResult() {
            return result;
        }
    }

    public void exec(String cmd) {
        try {
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(cmd);
            StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream(), "ERROR");
            StreamGobbler outputGobbler = new StreamGobbler(proc.getInputStream(), "OUTPUT");
            errorGobbler.start();
            outputGobbler.start();
            int exitVal = proc.waitFor();
            logger.error("ExitValue: " + exitVal);
            logger.error("errorGobbler" + errorGobbler.getResult());
            logger.error("outputGobbler" + outputGobbler.getResult());
        } catch (Throwable t) {
            logger.error("Throwable:" + t);
        }
    }
}
