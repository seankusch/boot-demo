package com.xe.demo;

import com.alibaba.fastjson.JSON;
import com.xe.demo.mapper.DataMapper;
import com.xe.demo.model.Data;

import java.io.IOException;
import java.net.*;

public class ThreadGetData implements Runnable {
    private DataMapper dataMapper;

    public ThreadGetData(DataMapper dataMapper) {
        this.dataMapper = dataMapper;
    }

    public DataMapper getDataMapper() {
        return dataMapper;
    }

    public void setDataMapper(DataMapper dataMapper) {
        this.dataMapper = dataMapper;
    }

    @Override
    public void run() {
        while (true) {
            //创建数据包传输对象DatagramSocket 绑定端口号
            DatagramSocket ds = null;
            try {
                ds = new DatagramSocket(1000);
            } catch (SocketException e) {
                e.printStackTrace();
            }
            //创建字节数组
            byte[] data = new byte[1024];
            //创建数据包对象，传递字节数组
            DatagramPacket dp = new DatagramPacket(data, data.length);
            //调用ds对象的方法receive传递数据包
            try {
                ds.receive(dp);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //获取发送端的IP地址对象
            String ip = dp.getAddress().getHostAddress();

            //获取发送的端口号
            int port = dp.getPort();

            //获取接收到的字节数
            int length = dp.getLength();
            String jsonStr = new String(data, 0, length);
            System.out.println(jsonStr + "...." + ip + ":" + port);
            Data dataObj = null;
            try {
                dataObj = JSON.parseObject(jsonStr, Data.class);
            } catch (Exception e) {
                System.out.println(e);
            }
            dataMapper.insert(dataObj);
//            System.out.println("OBJ >> " + dataObj.toString());

            ds.close();
        }
    }
}
