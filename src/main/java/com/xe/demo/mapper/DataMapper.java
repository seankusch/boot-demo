package com.xe.demo.mapper;

import com.xe.demo.common.dao.MyMapper;
import com.xe.demo.model.Data;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DataMapper extends MyMapper<Data> {
	Data queryById(@Param("id")String ID);

	List<Data> queryList(@Param("data") Data data);

	int insert(Data bean);
}