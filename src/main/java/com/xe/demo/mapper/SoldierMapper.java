package com.xe.demo.mapper;

import com.xe.demo.common.dao.MyMapper;
import com.xe.demo.model.Soldier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SoldierMapper extends MyMapper<Soldier> {
	
	Soldier queryById(@Param("id")Integer id);
	
	Soldier queryByName(@Param("name") String name);
	
	List<Soldier> queryList(@Param("soldier") Soldier soldier);
	
	int insert(Soldier bean);
	
	int update(Soldier bean);

}