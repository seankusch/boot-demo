package com.xe.demo.mapper;

import com.xe.demo.common.dao.MyMapper;
import com.xe.demo.model.Device;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DeviceMapper extends MyMapper<Device> {
	
	Device queryById(@Param("id")Integer id);
	
	Device queryBySn(@Param("sn") String sn);
	
	List<Device> queryList(@Param("device") Device device);
	
	int insert(Device bean);
	
	int update(Device bean);

}