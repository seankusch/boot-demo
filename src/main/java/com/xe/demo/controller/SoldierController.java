package com.xe.demo.controller;

import com.xe.demo.common.Constant;
import com.xe.demo.common.annotation.Authority;
import com.xe.demo.common.annotation.ControllerLog;
import com.xe.demo.common.pojo.AjaxResult;
import com.xe.demo.common.pojo.PageAjax;
import com.xe.demo.common.utils.DateUtil;
import com.xe.demo.model.AuthUser;
import com.xe.demo.model.Soldier;
import com.xe.demo.service.SoldierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping("/bx/class/")
public class SoldierController extends BaseController {

	@Autowired
	private SoldierService soldierService;

	@Authority(opCode = "0202", opName = "士兵界面")
	@RequestMapping("mainPage")
	public String mainPage() {
		return "bx/class/main";
	}

	@RequestMapping("queryPage")
	@ResponseBody
	@Authority(opCode = "0202", opName = "查询士兵列表")
	public PageAjax<Soldier> queryPage(PageAjax<Soldier> page, Soldier bean) {
		bean.setEnabled("1");
		return soldierService.queryPage(page, bean);
	}

	@Authority(opCode = "020201", opName = "添加士兵")
	@RequestMapping("addPage")
	public String addPage() {
		return "bx/class/add";
	}

	@ControllerLog("添加士兵")
	@RequestMapping("add")
	@ResponseBody
	@Authority(opCode = "020201", opName = "添加士兵")
	public AjaxResult add(Soldier bean) {
		AuthUser user = (AuthUser)getRequest().getSession().getAttribute(Constant.USER);
		if(user != null){
			bean.setCreateuser(String.valueOf(user.getId()));
		}
		return soldierService.saveBXClassify(bean);
	}

	@Authority(opCode = "020202", opName = "修改士兵页面")
	@RequestMapping("updatePage/{id}")
	public String updatePage(@PathVariable("id") int id, Map<String, Object> map) {
		Soldier bean = soldierService.queryBXClassifyById(id);
		map.put("classify", bean);
		return "bx/class/update";
	}

	@ControllerLog("修改士兵")
	@RequestMapping("update")
	@ResponseBody
	@Authority(opCode = "020202", opName = "修改士兵")
	public AjaxResult update(Soldier bean) {
		AuthUser user = (AuthUser)getRequest().getSession().getAttribute(Constant.USER);
		if(user != null){
			bean.setUpdateuser(String.valueOf(user.getId()));
			bean.setUpdatetime(DateUtil.getCurDate());
		}
		return soldierService.updateBXClassify(bean);
	}

	@ControllerLog("删除士兵")
	@RequestMapping("deleteByID/{id}")
	@ResponseBody
	@Authority(opCode = "020203", opName = "删除士兵")
	public AjaxResult deleteByID(@PathVariable("id") int id) {
		Soldier bean = new Soldier();
		bean.setId(id);
		bean.setEnabled("0");
		AuthUser user = (AuthUser)getRequest().getSession().getAttribute(Constant.USER);
		if(user != null){
			bean.setUpdateuser(String.valueOf(user.getId()));
			bean.setUpdatetime(DateUtil.getCurDate());
		}
		return soldierService.updateBXClassify(bean);
	}
}
