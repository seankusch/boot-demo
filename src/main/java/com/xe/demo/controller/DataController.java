package com.xe.demo.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xe.demo.ThreadGetData;
import com.xe.demo.common.FileBrowseUtil;
import com.xe.demo.common.annotation.Authority;
import com.xe.demo.common.annotation.ControllerLog;
import com.xe.demo.common.pojo.AjaxResult;
import com.xe.demo.mapper.DataMapper;
import com.xe.demo.model.Data;
import com.xe.demo.model.ModifyDeviceReq;
import com.xe.demo.model.PicDataRsp;
import com.xe.demo.service.DataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Random;

@Controller
@RequestMapping("/bx/data/")
public class DataController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(DataController.class);

    @Autowired
    private DataMapper dataMapper;

    @Value("${global.outputs.path}")
    private String outputPath;

    @Autowired
    private DataService dataService;

    @Authority(opCode = "0401", opName = "信息数据管理界面")
    @RequestMapping("mainPage")
    public String mainPage() {
        return "bx/data/main";
    }

    @ControllerLog("更新信息数据-测试")
    @RequestMapping("update/test")
    @ResponseBody
    public AjaxResult updateTest() {
        Data sampling = new Data();
        Random random = new Random();//以系统当前时间作为随机数生成的种子
        // 步数
        sampling.setSTP(random.nextInt(10000));
        // 压力
        sampling.setPRS(random.nextFloat() * 300);
        // 心率
        sampling.setHAT(random.nextInt(65) + 55);
        return new AjaxResult(sampling);
    }

    @ControllerLog("更新信息数据")
    @RequestMapping("update")
    @ResponseBody
    public AjaxResult update() {
        // 数据库中查询最新记录
        Data sampling = dataService.queryById("1");
        return new AjaxResult(sampling);
    }

    @ControllerLog("获取图片数据")
    @RequestMapping("pics/query")
    @ResponseBody
    public AjaxResult queryPics() {
        List<String> outputs = FileBrowseUtil.getFiles(outputPath);
        PicDataRsp result = new PicDataRsp();
        result.setOutputs(outputs);
        return new AjaxResult(result);
    }

    @ControllerLog("修改设备阈值")
    @RequestMapping("device/modify")
    @ResponseBody
    public AjaxResult modifyDevice(ModifyDeviceReq req) throws IOException {
        // "record/5/你好/";
        String cmd = req.getCmd();
        String deviceIp = req.getDeviceIp();
        int devicePort = req.getDevicePort();
        byte[] sentBuf = cmd.getBytes(StandardCharsets.UTF_8);
        DatagramSocket client = new DatagramSocket();
        InetAddress addr = InetAddress.getByName(deviceIp);
        DatagramPacket sendPacket
                = new DatagramPacket(sentBuf, sentBuf.length, addr, devicePort);
        client.send(sendPacket);
        client.close();
        return new AjaxResult();
    }

    @ControllerLog("开始接受数据")
    @RequestMapping("device/receive")
    @ResponseBody
    public AjaxResult receive() throws IOException {
        logger.info("starting data receive");
        Thread thread = new Thread(new ThreadGetData(dataMapper));
        thread.start();
        logger.info("started data receive");
        return new AjaxResult();
    }
}
