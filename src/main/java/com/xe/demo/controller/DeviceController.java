package com.xe.demo.controller;

import com.xe.demo.common.Constant;
import com.xe.demo.common.annotation.Authority;
import com.xe.demo.common.annotation.ControllerLog;
import com.xe.demo.common.pojo.AjaxResult;
import com.xe.demo.common.pojo.PageAjax;
import com.xe.demo.common.utils.DateUtil;
import com.xe.demo.model.AuthUser;
import com.xe.demo.model.Device;
import com.xe.demo.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping("/bx/device/")
public class DeviceController extends BaseController {
	@Autowired
	private DeviceService deviceService;

	@Authority(opCode = "0201", opName = "设备界面")
	@RequestMapping("mainPage")
	public String mainPage() {
		return "bx/dvs/main";
	}

	@RequestMapping("queryPage")
	@ResponseBody
	@Authority(opCode = "0201", opName = "查询设备列表")
	public PageAjax<Device> queryPage(PageAjax<Device> page, Device bean) {
		bean.setEnabled("1");
		return deviceService.queryPage(page, bean);
	}

	@Authority(opCode = "020101", opName = "添加设备")
	@RequestMapping("addPage")
	public String addPage() {
		return "bx/dvs/add";
	}

	@ControllerLog("添加设备")
	@RequestMapping("add")
	@ResponseBody
	@Authority(opCode = "020101", opName = "添加设备")
	public AjaxResult add(Device bean) {
		AuthUser user = (AuthUser)getRequest().getSession().getAttribute(Constant.USER);
		if(user != null){
			bean.setCreateuser(String.valueOf(user.getId()));
		}
		return deviceService.saveDevice(bean);
	}

	@Authority(opCode = "020102", opName = "修改设备页面")
	@RequestMapping("updatePage/{id}")
	public String updatePage(@PathVariable("id") int id, Map<String, Object> map) {
		Device bean = deviceService.queryDeviceById(id);
		map.put("device", bean);
		return "bx/dvs/update";
	}

	@ControllerLog("修改设备")
	@RequestMapping("update")
	@ResponseBody
	@Authority(opCode = "020102", opName = "修改设备")
	public AjaxResult update(Device bean) {
		AuthUser user = (AuthUser)getRequest().getSession().getAttribute(Constant.USER);
		if(user != null){
			bean.setUpdateuser(String.valueOf(user.getId()));
			bean.setUpdatetime(DateUtil.getCurDate());
		}
		return deviceService.updateDevice(bean);
	}

	@ControllerLog("删除设备")
	@RequestMapping("deleteByID/{id}")
	@ResponseBody
	@Authority(opCode = "010203", opName = "删除设备")
	public AjaxResult deleteByID(@PathVariable("id") int id) {
		Device bean = new Device();
		bean.setId(id);
		bean.setEnabled("0");
		AuthUser user = (AuthUser)getRequest().getSession().getAttribute(Constant.USER);
		if(user != null){
			bean.setUpdateuser(String.valueOf(user.getId()));
			bean.setUpdatetime(DateUtil.getCurDate());
		}
		return deviceService.updateDevice(bean);
	}
}
