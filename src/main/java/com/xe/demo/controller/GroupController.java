package com.xe.demo.controller;

import com.xe.demo.common.annotation.Authority;
import com.xe.demo.common.annotation.ControllerLog;
import com.xe.demo.common.pojo.AjaxResult;
import com.xe.demo.model.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Random;

@Controller
@RequestMapping("/bx/group/")
public class GroupController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);

    @Authority(opCode = "0401", opName = "信息数据管理界面")
    @RequestMapping("mainPage")
    public String mainPage() {
        return "bx/group/main";
    }

    @ControllerLog("更新信息数据-测试")
    @RequestMapping("update")
    @ResponseBody
    public AjaxResult updateTest() {
        Data sampling = new Data();
        Random random = new Random();//以系统当前时间作为随机数生成的种子
        // 步数
        sampling.setSTP(random.nextInt(10000));
        // 压力
        sampling.setPRS(random.nextFloat() * 300);
        // 心率
        sampling.setHAT(random.nextInt(65) + 55);
        return new AjaxResult(sampling);
    }
}
