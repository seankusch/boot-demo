package com.xe.demo.service;

import com.github.pagehelper.page.PageMethod;
import com.xe.demo.common.annotation.ServiceLog;
import com.xe.demo.common.pojo.AjaxResult;
import com.xe.demo.common.pojo.PageAjax;
import com.xe.demo.common.utils.AppUtil;
import com.xe.demo.mapper.DeviceMapper;
import com.xe.demo.model.Device;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeviceService extends AbstratService<Device> {

	@Autowired
	private DeviceMapper deviceMapper;
	
	public Device queryDeviceById(int id){
		return deviceMapper.queryById(id);
	}
	
	public List<Device> queryList(Device bean){
		return deviceMapper.queryList(bean);
	}
	
	@Override
	@ServiceLog("查询设备列表")
	public PageAjax<Device> queryPage(PageAjax<Device> page, Device bean) {
		PageMethod.startPage(page.getPageNo(), page.getPageSize());
		List<Device> list = deviceMapper.queryList(bean);
		return AppUtil.returnPage(list);
	}

	@ServiceLog("新增设备")
	public AjaxResult saveDevice(Device bean) {
		String result;
		Device device = deviceMapper.queryBySn(bean.getSn());
		if (null == device) {
			bean.setEnabled("1");
			result = deviceMapper.insert(bean) > 0 ? null : "操作失败";
		} else {
			result = "设备已存在";
		}
		return AppUtil.returnObj(result);
	}

	@ServiceLog("更新设备")
	public AjaxResult updateDevice(Device bean) {
		String result = null;
		Device device = deviceMapper.queryBySn(bean.getSn());
		if (null != device && device.getId() != bean.getId()) {
			result = "设备已存在";
		} else {
			deviceMapper.update(bean);
		}
		return AppUtil.returnObj(result);
	}
}
