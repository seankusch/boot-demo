package com.xe.demo.service;

import com.github.pagehelper.page.PageMethod;
import com.xe.demo.common.annotation.ServiceLog;
import com.xe.demo.common.pojo.AjaxResult;
import com.xe.demo.common.pojo.PageAjax;
import com.xe.demo.common.utils.AppUtil;
import com.xe.demo.mapper.DataMapper;
import com.xe.demo.model.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DataService extends AbstratService<Data> {
    @Autowired
    private DataMapper dataMapper;

    @Override
    @ServiceLog("查询数据列表")
    public PageAjax<Data> queryPage(PageAjax<Data> page, Data bean) {
        PageMethod.startPage(page.getPageNo(), page.getPageSize());
        List<Data> list = dataMapper.queryList(bean);
        return AppUtil.returnPage(list);
    }

    @ServiceLog("查寻数据")
    public Data queryById(String id) {
        return dataMapper.queryById(id);
    }

    @ServiceLog("新增数据")
    public AjaxResult save(Data bean) {
        String result = dataMapper.insert(bean) > 0 ? null : "操作失败";
        return AppUtil.returnObj(result);
    }


}
