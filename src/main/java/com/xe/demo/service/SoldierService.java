package com.xe.demo.service;

import com.github.pagehelper.page.PageMethod;
import com.xe.demo.common.annotation.ServiceLog;
import com.xe.demo.common.pojo.AjaxResult;
import com.xe.demo.common.pojo.PageAjax;
import com.xe.demo.common.utils.AppUtil;
import com.xe.demo.mapper.SoldierMapper;
import com.xe.demo.model.Soldier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SoldierService extends AbstratService<Soldier> {

	@Autowired
	private SoldierMapper soldierMapper;
	
	public Soldier queryBXClassifyById(int id){
		return soldierMapper.queryById(id);
	}
	
	public List<Soldier> queryList(Soldier bean){
		return soldierMapper.queryList(bean);
	}
	
	@Override
	@ServiceLog("查询士兵列表")
	public PageAjax<Soldier> queryPage(PageAjax<Soldier> page, Soldier bean) {
		PageMethod.startPage(page.getPageNo(), page.getPageSize());
		List<Soldier> list = soldierMapper.queryList(bean);
		return AppUtil.returnPage(list);
	}

	@ServiceLog("新增士兵")
	public AjaxResult saveBXClassify(Soldier bean) {
		String result = null;
		Soldier soldier = soldierMapper.queryByName(bean.getName());
		if (null == soldier) {
			bean.setEnabled("1");
			result = soldierMapper.insert(bean) > 0 ? null : "操作失败";
		} else {
			result = "士兵已存在";
		}
		return AppUtil.returnObj(result);
	}

	@ServiceLog("更新士兵")
	public AjaxResult updateBXClassify(Soldier bean) {
		String result = null;
		Soldier soldier = soldierMapper.queryByName(bean.getName());
		if (null != soldier && soldier.getId() != bean.getId()) {
			result = "士兵已存在";
		} else {
			soldierMapper.update(bean);
		}
		return AppUtil.returnObj(result);
	}
}
