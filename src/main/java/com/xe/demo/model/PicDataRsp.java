package com.xe.demo.model;

import java.util.List;

public class PicDataRsp {
    private List<String> outputs;

    public List<String> getOutputs() {
        return outputs;
    }

    public void setOutputs(List<String> outputs) {
        this.outputs = outputs;
    }
}
