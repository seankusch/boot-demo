package com.xe.demo.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tbl_data")
public class Data {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private transient Integer DATA_ID;

    // 设备ID
    private String ID;

    // 采样序号
    private int SEQ;

    // 采样序号
    private long UUID;


    // 加速度
    private float ACC_X;
    private float ACC_Y;
    private float ACC_Z;

    // 角速度
    private float GRY_X;
    private float GRY_Y;
    private float GRY_Z;

    //磁力计
    private float MAG_X;
    private float MAG_Y;
    private float MAG_Z;

    // 压力
    private float PRS;

    // 温度
    private float TMP;

    // 湿度
    private float HMD;

    // 光照
    private float LX;

    // 心率
    private int HAT;

    // 血糖
    private float BLS;

    // 血氧
    private float BLO;

    // 血压(低)
    private float BLP_L;
    // 血压(高)
    private float BLP_H;

    // 步数
    private int STP;

    // 卡路里
    private float CAL;

    // GPS
    private String GPS_1;
    private String GPS_2;

    public Integer getDATA_ID() {
        return DATA_ID;
    }

    public void setDATA_ID(Integer DATA_ID) {
        this.DATA_ID = DATA_ID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public int getSEQ() {
        return SEQ;
    }

    public void setSEQ(int SEQ) {
        this.SEQ = SEQ;
    }

    public long getUUID() {
        return UUID;
    }

    public void setUUID(long UUID) {
        this.UUID = UUID;
    }

    public float getACC_X() {
        return ACC_X;
    }

    public void setACC_X(float ACC_X) {
        this.ACC_X = ACC_X;
    }

    public float getACC_Y() {
        return ACC_Y;
    }

    public void setACC_Y(float ACC_Y) {
        this.ACC_Y = ACC_Y;
    }

    public float getACC_Z() {
        return ACC_Z;
    }

    public void setACC_Z(float ACC_Z) {
        this.ACC_Z = ACC_Z;
    }

    public float getGRY_X() {
        return GRY_X;
    }

    public void setGRY_X(float GRY_X) {
        this.GRY_X = GRY_X;
    }

    public float getGRY_Y() {
        return GRY_Y;
    }

    public void setGRY_Y(float GRY_Y) {
        this.GRY_Y = GRY_Y;
    }

    public float getGRY_Z() {
        return GRY_Z;
    }

    public void setGRY_Z(float GRY_Z) {
        this.GRY_Z = GRY_Z;
    }

    public float getMAG_X() {
        return MAG_X;
    }

    public void setMAG_X(float MAG_X) {
        this.MAG_X = MAG_X;
    }

    public float getMAG_Y() {
        return MAG_Y;
    }

    public void setMAG_Y(float MAG_Y) {
        this.MAG_Y = MAG_Y;
    }

    public float getMAG_Z() {
        return MAG_Z;
    }

    public void setMAG_Z(float MAG_Z) {
        this.MAG_Z = MAG_Z;
    }

    public float getPRS() {
        return PRS;
    }

    public void setPRS(float PRS) {
        this.PRS = PRS;
    }

    public float getTMP() {
        return TMP;
    }

    public void setTMP(float TMP) {
        this.TMP = TMP;
    }

    public float getHMD() {
        return HMD;
    }

    public void setHMD(float HMD) {
        this.HMD = HMD;
    }

    public float getLX() {
        return LX;
    }

    public void setLX(float LX) {
        this.LX = LX;
    }

    public int getHAT() {
        return HAT;
    }

    public void setHAT(int HAT) {
        this.HAT = HAT;
    }

    public float getBLS() {
        return BLS;
    }

    public void setBLS(float BLS) {
        this.BLS = BLS;
    }

    public float getBLO() {
        return BLO;
    }

    public void setBLO(float BLO) {
        this.BLO = BLO;
    }

    public float getBLP_L() {
        return BLP_L;
    }

    public void setBLP_L(float BLP_L) {
        this.BLP_L = BLP_L;
    }

    public float getBLP_H() {
        return BLP_H;
    }

    public void setBLP_H(float BLP_H) {
        this.BLP_H = BLP_H;
    }

    public int getSTP() {
        return STP;
    }

    public void setSTP(int STP) {
        this.STP = STP;
    }

    public float getCAL() {
        return CAL;
    }

    public void setCAL(float CAL) {
        this.CAL = CAL;
    }

    public String getGPS_1() {
        return GPS_1;
    }

    public void setGPS_1(String GPS_1) {
        this.GPS_1 = GPS_1;
    }

    public String getGPS_2() {
        return GPS_2;
    }

    public void setGPS_2(String GPS_2) {
        this.GPS_2 = GPS_2;
    }

    @Override
    public String toString() {
        return "Data{" +
                "DATA_ID=" + DATA_ID +
                ", ID='" + ID + '\'' +
                ", SEQ=" + SEQ +
                ", UUID=" + UUID +
                ", ACC_X=" + ACC_X +
                ", ACC_Y=" + ACC_Y +
                ", ACC_Z=" + ACC_Z +
                ", GRY_X=" + GRY_X +
                ", GRY_Y=" + GRY_Y +
                ", GRY_Z=" + GRY_Z +
                ", MAG_X=" + MAG_X +
                ", MAG_Y=" + MAG_Y +
                ", MAG_Z=" + MAG_Z +
                ", PRS=" + PRS +
                ", TMP=" + TMP +
                ", HMD=" + HMD +
                ", LX=" + LX +
                ", HAT=" + HAT +
                ", BLS=" + BLS +
                ", BLO=" + BLO +
                ", BLP_L=" + BLP_L +
                ", BLP_H=" + BLP_H +
                ", STP=" + STP +
                ", CAL=" + CAL +
                ", GPS_1='" + GPS_1 + '\'' +
                ", GPS_2='" + GPS_2 + '\'' +
                '}';
    }
}

